FROM rabbitmq:3-management

WORKDIR /TelecomRabbitMQ

COPY TP/ /TelecomRabbitMQ

RUN apt-get update
RUN apt-get dist-upgrade -y

RUN useradd -u 1001 non-root
USER non-root

EXPOSE 15672
EXPOSE 5672